# Punk Project

ReactJs, NodeJs and Express demo.

## Getting Started

To get started with this project follow these steps:
Step 1: Clone the repo to a folder using: git clone https://github.com/SwyserDev/punk-project.git
Step 2: Run the command: npm install
Step 3: Run the following gulp commands in the exact same order:
    gulp clean
    gulp build
    gulp client-gsw
    gulp run
Step 4: Sit back and enjoy the app :D

### Prerequisites

Node package manager (npm) is required to run this project.

## Deployment

The Dockerfile that is included needs to be changed, please do not use.

## Versioning

Who knows...

## Authors

Francois van der Merwe (swyser.co.za)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

The solo adventures of a midnight code scrolling geek...