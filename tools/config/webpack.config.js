var config = { 
  client: {
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader'
      }
    ]
  },
  output: {
    filename: 'app.js'
  }
},
server: {}
};

module.exports = config;