var gulp = require('gulp');

gulp.task('server-build', [
    'server-minify-html',
    'server-js',
    'server-json'
]);