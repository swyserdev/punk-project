var gulp = require('gulp');

gulp.task('server-json', function() {
  gulp.src('src/server/*.json')
    .pipe(gulp.dest('dist/server'));
});