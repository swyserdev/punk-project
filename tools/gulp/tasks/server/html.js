var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');

gulp.task('server-minify-html', function() {
  gulp.src('src/server/index.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('dist/server'));
});