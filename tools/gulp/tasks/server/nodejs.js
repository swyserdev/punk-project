var gulp = require('gulp');
var nodemon = require('gulp-nodemon');

gulp.task('server-server', function () {
  nodemon({
    script: 'dist/server/server.js',
    ext: 'js html',
    watch: 'dist/server'
  });
});