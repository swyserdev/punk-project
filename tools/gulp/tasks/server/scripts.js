var gulp = require('gulp');
var uglify = require('gulp-uglify');

gulp.task('server-js', function() {
  gulp.src('src/server/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('dist/server'));
});