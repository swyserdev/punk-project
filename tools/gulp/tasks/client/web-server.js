var gulp = require('gulp');
var webserver = require('gulp-webserver');

gulp.task('client-webserver', function () {
  gulp.src('dist/client/')
    .pipe(webserver({
      fallback: 'index.html',
      open: true,
      directoryListing: {
        enable: true,
        path: 'public'
      },
      livereload: true
    }));
});