var gulp = require('gulp');
var cssmin = require('gulp-cssmin');

gulp.task('client-minify-css', function() {
  gulp.src('src/client/css/*.css')
    .pipe(cssmin())
    .pipe(gulp.dest('dist/client/css'));
});