var gulp = require('gulp');

gulp.task('client-favicon', function() {
  gulp.src('src/client/favicon.ico')
    .pipe(gulp.dest('dist/client'));
});