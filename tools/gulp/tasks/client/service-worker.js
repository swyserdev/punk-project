var gulp = require('gulp');
var swPrecache = require('sw-precache');

gulp.task('client-gsw', function(callback) {
  swPrecache.write('dist/service-worker.js', {
    cacheId: 'ConferenceIn.',
    staticFileGlobs: [
      'dist/client/**/*.{js,html,css,png,svg,jpg,gif,json}'
    ],
    stripPrefix: 'dist/client'
  }, callback);
});