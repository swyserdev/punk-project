var gulp = require('gulp');

gulp.task('client-images', function() {
  gulp.src('src/client/assets/images/**/*.*')
    .pipe(gulp.dest('dist/client/images/'));
});