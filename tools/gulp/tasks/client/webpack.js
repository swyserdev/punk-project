var gulp = require('gulp');
var webpack = require("webpack-stream");
var appRoot = require('app-root-path');

gulp.task('client-webpack', function () {
  return gulp.src([appRoot.path + '/src/client/app/app.js'])
    .pipe(webpack(require('../../../config/webpack.config.js').client))
    .pipe(gulp.dest(appRoot.path + '/dist/client'));
});