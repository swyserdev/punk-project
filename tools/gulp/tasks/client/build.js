var gulp = require('gulp');

gulp.task('client-build', [
    'client-favicon',
    'client-minify-html',
    'client-images',
    'client-webpack',
    'client-mainfest'
]);