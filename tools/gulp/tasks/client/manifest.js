var gulp = require('gulp');

gulp.task('client-mainfest', function() {
  return gulp.src('src/client/manifest.json')
    .pipe(gulp.dest('dist/client'));
});