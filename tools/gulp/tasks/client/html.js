var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');

gulp.task('client-minify-html', function() {
  gulp.src('src/client/index.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('dist/client'));
});