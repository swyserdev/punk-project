var gulp = require('gulp');
var requireDir = require('require-dir');

requireDir('./tools/gulp/tasks', { recurse: true });

gulp.task('build', [
  'client-build',
  'server-build'
]);

gulp.task('run', [
  'client-webserver', 
  'server-server'
]);