import React from 'react';
import ReactDOM from "react-dom";
import { Router, Route, browserHistory, IndexRoute } from 'react-router';
import { Provider } from 'react-redux';
import Store from './framework/redux/store';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();
import { HomeContainer, GuestContainer, LoginContainer, AppContainer } from './containers';

const AppMain = (
    <Provider store={Store}>
        <MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme)}>
            <Router history={browserHistory}>
                <Route path={"/"} component={AppContainer}>
                    <IndexRoute component={HomeContainer} />
                    <Route path="home" component={HomeContainer}></Route>
                    <Route path="guest" component={GuestContainer}></Route>
                </Route>
                <Route path={"login"} component={LoginContainer} />
            </Router>
        </MuiThemeProvider>
    </Provider>
);

ReactDOM.render(AppMain, document.getElementById('app'));