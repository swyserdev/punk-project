import axios from 'axios';

export function fetchGuestsAction() {
  return function (dispatch) {
    axios.get("http://localhost:8080/api/")
      .then((response) => {
        dispatch({ type: "FETCH_GUESTS_FULFILLED", payload: response.data.guests });
      })
      .catch((err) => {
        dispatch({ type: "FETCH_GUESTS_REJECTED", payload: err });
      });
  }
}

export function setSelectedGuestAction(guest) {
  return function (dispatch) {
    dispatch({ type: "SET_SELECTED_GUEST", payload: guest })
  }
} 