const initialState = {
  fetching: false,
  fetched: false,
  error: null,
  guests: [],
  selectedGuest: {}
}

const GuestReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_GUESTS":
      return {
        ...state,
        fetching: true
      }
      break;
    case "FETCH_GUESTS_REJECTED":
      return {
        ...state,
        fetching: false,
        error: action.payload
      }
      break;
    case "FETCH_GUESTS_FULFILLED":
      state = {
        ...state,
        fetching: false,
        fetched: true,
        guests: action.payload
      };
      break;
    case "SET_SELECTED_GUEST":
      state = {
        ...state,
        selectedGuest: action.payload
      };
      break;
  }
  return state;
};

export default GuestReducer;