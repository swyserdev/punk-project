import { combineReducers } from "redux";

import AuthReducer from './auth.reducer';
import GuestReducer from './guest.reducer';
import NavigatorReducer from './navigation.reducer';

export default combineReducers({
  AuthReducer,
  GuestReducer,
  NavigatorReducer
});