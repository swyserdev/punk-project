import React from 'react';
import { connect } from 'react-redux';
import { fetchGuestsAction, setSelectedGuestAction } from '../framework/redux/actions';
import { AddGuestComponent, GuestListComponent } from '../components';
import Box, { Page, Container, VBox } from 'react-layout-components';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';

const styles = {
  mainContainer: {
    margin: 20
  }
}

class GuestContainer extends React.Component {
  componentWillMount() {
    this.props.dispatch(fetchGuestsAction());
  }

  selectGuest(guest) {
    this.props.dispatch(setSelectedGuestAction(guest));
  }

  render() {
    const { guests } = this.props;

    return (
      <Paper>
      <Box fit column>
        <Box flex="0 0 auto" justifyContent="center" alignContent="center">
          <Box width="1000px" height="600px" style={styles.mainContainer}>
            <GuestListComponent guestList={this.props.guests} selectGuest={this.selectGuest.bind(this)} />
            <Box width="100%" height="100%" justifyContent="center" alignContent="center">
              <AddGuestComponent selectedGuest={this.props.selectedGuest} />
            </Box>
          </Box>
        </Box>
      </Box>
      </Paper>
    );
  }
}

export default connect((store) => {
  return {
    guests: store.GuestReducer.guests,
    selectedGuest: store.GuestReducer.selectedGuest
  }
})(GuestContainer);