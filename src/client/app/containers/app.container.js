import React from 'react';
import { AppLayoutComponent } from '../framework/ui';

const style = {
  container: {
    margin: 20
  }
};

class AppContainer extends React.Component {
  render() {
    return (
      <div>
        <AppLayoutComponent />
        {this.props.children}
      </div>
    );
  }
}

export default AppContainer;