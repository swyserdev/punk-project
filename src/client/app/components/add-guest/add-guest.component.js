import React from 'react';
import { Box } from 'react-layout-components';
import { CardActions, Card, CardText, CardHeader } from 'material-ui/Card';
import Divider from 'material-ui/Divider';
import RaisedButton from 'material-ui/FlatButton';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import { AppNotificationComponent } from '../../framework/ui';
import TextField from 'material-ui/TextField';

const styles = {
  card: {
    visibility: 'visible',
    marginTop: 150
  },
  cardHidden: {
    visibility: 'hidden'
  },
  cardActions: {
    marginTop: 10
  }
};

class AddGuestComponent extends React.Component {
  render() {
    const { selectedGuest } = this.props;

    return (
      <Box width="400px" height="500px" style={styles.blackBox}>
        <Card style={this.props.selectedGuest.first_name !== undefined ? styles.card : styles.cardHidden}>
          <CardHeader
            title={selectedGuest.first_name + ' ' + selectedGuest.last_name}
            subtitle={selectedGuest.company}
            avatar="images/userImg.png"
          />
          <Divider />
          <CardText>
            <Box>
              <h4>Contact Details:</h4>
            </Box>
            <Box>
              <TextField
                floatingLabelText="Email address"
              />
            </Box>
            <Box>
              <TextField
                floatingLabelText="Mobile number"
              />
            </Box>
            <Box>
              <FlatButton
                label="Add number"
                icon={<FontIcon className="add_circle_outline" />}
              />
            </Box>
          </CardText>
          <Divider />
          <CardActions style={ styles.cardActions }>
            <Box justifyContent="center" alignContent="center">
              <RaisedButton label="Save details" primary={true} />
            </Box>
          </CardActions>
        </Card>
      </Box>
    );
  }
}

export default AddGuestComponent;