import React from 'react';

import Box from 'react-layout-components';

const styles = {
  statusContainer: {
    margin: 15
  }
};

class GuestListStatusComponent extends React.Component {
  render() {
    const { guestCount } = this.props;

    return (
      <Box width="100%" style={ styles.statusContainer }>
        { guestCount } Confirmed Guests
      </Box>
    );
  }
}

export default GuestListStatusComponent;