import React from 'react';
import { List } from 'material-ui/List';
import { GuestListItemComponent } from './index';
import { ScrollView } from 'react-layout-components';

class GuestListRepeaterComponent extends React.Component {
  render() {
    const { guestList } = this.props;

    if (!guestList.length) {
      return <div>No Guests!</div>;
    }

    const mappedGuests = guestList.map((guest, i) => <GuestListItemComponent key={i} guest={guest} selectGuest={this.props.selectGuest} />);
    return (
      <ScrollView width="100%" flex={1}>
        <List>
          {mappedGuests}
        </List>
      </ScrollView>
    );
  }
}

export default GuestListRepeaterComponent;