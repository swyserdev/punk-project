import React from 'react';
import TextField from 'material-ui/TextField';
import Box from 'react-layout-components';

const styles = {
  searchContainer: {
    margin: 15
  }
};

class GuestListSearchComponent extends React.Component {
  render() {
    return (
      <Box width="100%" style={ styles.searchContainer }>
        <TextField hintText="Search..." />
      </Box>
    );
  }
}

export default GuestListSearchComponent;