import React from 'react';
import { GuestListSearchComponent, GuestListStatusComponent, GuestListRepeaterComponent } from './';
import { VBox } from 'react-layout-components';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';

class GuestListComponent extends React.Component {
  render() {
    const { guestList } = this.props;
    return (
      <VBox width="100%">
        <GuestListSearchComponent />
        <Divider />
        <GuestListStatusComponent guestCount={guestList.length} />
        <GuestListRepeaterComponent guestList={guestList} selectGuest={this.props.selectGuest} />
      </VBox>
    );
  }
}

export default GuestListComponent;