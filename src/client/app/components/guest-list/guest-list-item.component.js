import React from 'react';
import Avatar from 'material-ui/Avatar';
import { ListItem } from 'material-ui/List';

class GuestListItemComponent extends React.Component {
  selectGuest() {
    this.props.selectGuest(this.props.guest)
  }

  render() {
    const { guest } = this.props;

    return (
      <ListItem
        primaryText={guest.first_name + ' ' + guest.last_name}
        leftAvatar={<Avatar src="images/userImg.png" />}
        onClick={() => this.selectGuest()}
      />
    );
  }
}

export default GuestListItemComponent;