(function () {
    'use strict';

    var express = require('express');
    var guestRoutes = express.Router();
    var guestsJSON = require('./guestsDb');

    guestRoutes.get('/', function (req, res) {
        res.status(200).json(guestsJSON);
    });

    module.exports = {
        routes: function () {
            return guestRoutes;
        }
    };
})();
