// Library imports
var express = require('express');
var app = express();
var http = require('http');
var bodyParser = require('body-parser');
var path = require('path');

// Custom module imports
var guestRoutes = require('./guestRoutes.js');

var router = express.Router();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', function (req, res) {
  res.sendFile(path.resolve('src/server/index.html'));
});

app.use('/api', guestRoutes.routes());

app.listen(8080);
console.log('Server running on port 8080...');
